package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
)

type MakeObj struct {
	Path      string
	Target    string
	HowToMake func(string, string) string
}

type Makefile struct {
	Folder       string
	Target       string
	HowToInstall func(string) string
	objects      []*MakeObj
}

type MakeInstruction struct {
	Target       string
	Dependancies []string
	HowTo        string
}

func (l *MakeInstruction) WriteTo(w io.Writer) {
	fmt.Fprintf(w, "%s: %s\n", l.Target, strings.Join(l.Dependancies, " "))
	fmt.Fprintf(w, "\t%s\n", l.HowTo)
}

func (makefile *Makefile) Make() {
	f, err := os.Create("Makefile")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close()
	filepath.Walk(makefile.Folder, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		ext := filepath.Ext(path)
		objPath := strings.Replace(strings.Replace(path, filepath.Ext(path), "", -1)+".o", makefile.Folder+"/", "build/", -1)
		if ext == ".s" || ext == ".asm" {
			makefile.objects = append(makefile.objects, &MakeObj{path, objPath, makeAsm})
		} else if ext == ".c" {
			makefile.objects = append(makefile.objects, &MakeObj{path, objPath, makeC})
		}
		return nil
	})
	linkInstruction := &MakeInstruction{
		Target: "xos-kernel",
		HowTo:  "ld -m elf_i386 -T link.ld -o xos-kernel",
	}
	compileInstructions := []*MakeInstruction{}
	for _, obj := range makefile.objects {
		linkInstruction.Dependancies = append(linkInstruction.Dependancies, obj.Target)
		linkInstruction.HowTo += " " + obj.Target
		compileInstructions = append(compileInstructions, &MakeInstruction{
			Target:       obj.Target,
			Dependancies: []string{obj.Path},
			HowTo:        obj.HowToMake(obj.Path, obj.Target),
		})
	}
	linkInstruction.WriteTo(f)
	for _, ins := range compileInstructions {
		ins.WriteTo(f)
	}
	installInstruction := &MakeInstruction{
		Target:       "install",
		Dependancies: []string{makefile.Target},
		HowTo:        makefile.HowToInstall(makefile.Target),
	}
	installInstruction.WriteTo(f)
	cleanInstruction := &MakeInstruction{
		Target: "clean",
		HowTo:  "rm -rf build " + makefile.Target,
	}
	cleanInstruction.WriteTo(f)
}

func makeAsm(path string, objPath string) string {
	return fmt.Sprintf("mkdir -p %s && nasm -f elf32 %s -o %s", filepath.Dir(objPath), path, objPath)
}

func makeC(path string, objPath string) string {
	return fmt.Sprintf("mkdir -p %s && gcc -m32 -c %s -o %s", filepath.Dir(objPath), path, objPath)
}

func main() {
	makefile := &Makefile{
		Folder: "src",
		Target: "xos-kernel",
		HowToInstall: func(target string) string {
			return fmt.Sprintf("cp %s /mnt/xos/boot", target)
		},
	}
	makefile.Make()
}

xos-kernel: build/common/asm.o build/common/terminal.o build/main.o build/multiboot.o build/setup/func.o build/setup/gdt.o build/setup/interrupt.o build/setup/paging.o
	ld -m elf_i386 -T link.ld -o xos-kernel build/common/asm.o build/common/terminal.o build/main.o build/multiboot.o build/setup/func.o build/setup/gdt.o build/setup/interrupt.o build/setup/paging.o
build/common/asm.o: src/common/asm.c
	mkdir -p build/common && gcc -m32 -c src/common/asm.c -o build/common/asm.o
build/common/terminal.o: src/common/terminal.c
	mkdir -p build/common && gcc -m32 -c src/common/terminal.c -o build/common/terminal.o
build/main.o: src/main.c
	mkdir -p build && gcc -m32 -c src/main.c -o build/main.o
build/multiboot.o: src/multiboot.s
	mkdir -p build && nasm -f elf32 src/multiboot.s -o build/multiboot.o
build/setup/func.o: src/setup/func.s
	mkdir -p build/setup && nasm -f elf32 src/setup/func.s -o build/setup/func.o
build/setup/gdt.o: src/setup/gdt.c
	mkdir -p build/setup && gcc -m32 -c src/setup/gdt.c -o build/setup/gdt.o
build/setup/interrupt.o: src/setup/interrupt.c
	mkdir -p build/setup && gcc -m32 -c src/setup/interrupt.c -o build/setup/interrupt.o
build/setup/paging.o: src/setup/paging.c
	mkdir -p build/setup && gcc -m32 -c src/setup/paging.c -o build/setup/paging.o
install: xos-kernel
	cp xos-kernel /mnt/xos/boot
clean: 
	rm -rf build xos-kernel

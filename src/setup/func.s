bits 32

[global switch0]
ALIGN 4
switch0:
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	jmp 0x08:flush0
flush0:
	ret

[global switch3]
ALIGN 4
switch3:
	mov ax, 0x20
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	jmp 0x18:flush3
flush3:
	ret

[global isr_wrapper]
extern interrupt_handler
ALIGN 4
isr_wrapper:
	pushad
	call interrupt_handler
	popad
	iret

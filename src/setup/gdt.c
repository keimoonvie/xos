#include <stdint.h>
#include "../common/terminal.h"
#include "gdt.h"

#define GDT_SIZE 8
#define GDT_NUM 5
#define GDT_TOTAL_SIZE GDT_SIZE * GDT_NUM

struct GDT {
	uint32_t base;
	uint32_t limit;
	uint8_t type;
};

struct gdt_ptr {
	uint16_t len;
	uint32_t ptr;
} __attribute__((packed));

void write_gdt(uint8_t *dest, struct GDT *gdt) {
	dest[0] = gdt->limit & 0xFF;
	dest[1] = (gdt->limit >> 8) &0xFF;
	dest[2] = gdt->base & 0xFF;
	dest[3] = (gdt->base >> 8) & 0xFF;
        dest[4] = (gdt->base >> 16) & 0xFF;
	dest[5] = gdt->type;
	dest[6] = 0xc0 | (gdt->limit >> 16) & 0x0F;
	dest[7] = (gdt->base >> 24) & 0xFF;
}

void setup_gdt() {
	static uint8_t system_gdt[GDT_TOTAL_SIZE];
	static struct gdt_ptr gdtr;
	struct GDT gdt = {.base = 0, .limit = 0, .type = 0};
	write_gdt(system_gdt, &gdt);
	gdt.base = 0;
	gdt.limit = 0xfffff;
	gdt.type = 0x9A; // Code segment, ring 0
	write_gdt(system_gdt + 0x08, &gdt);
	gdt.type = 0x92; // Data segment, ring 0
	write_gdt(system_gdt + 0x10, &gdt);
	gdt.type = 0xFA; // Code segment, ring 3
	write_gdt(system_gdt + 0x18, &gdt);
	gdt.type = 0xF2; // Data segment, ring 3
	write_gdt(system_gdt + 0x20, &gdt);
	gdtr.len = GDT_TOTAL_SIZE - 1;
	gdtr.ptr = (uint32_t) &system_gdt;
	asm volatile("lgdtl %0" : : "m" (gdtr));
	switch0();
}

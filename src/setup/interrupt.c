#include "../common/asm.h"
#include "../common/terminal.h"
#include "interrupt.h"

#define PIC1_COMMAND 0x20
#define PIC1_DATA    0x21
#define PIC2_COMMAND 0xA0
#define PIC2_DATA    0xA1
#define PIC_EOI      0x20
#define PIC_READ_IRR 0x0a
#define PIC_READ_ISR 0x0b

#define IDT_SIZE 8
#define IDT_NUM 48
#define IDT_TOTAL_SIZE IDT_SIZE * IDT_NUM

struct IDT {
	uint32_t handler;
        uint16_t selector;
	uint8_t type;
};

struct idt_ptr {
	uint16_t len;
	uint32_t ptr;
} __attribute__((packed));

void setup_intel8259() {
	outb(PIC1_COMMAND, 0x11);
	io_wait();
	outb(PIC2_COMMAND, 0x11); //ICW1:  Init, cascade mode, has ICW4
	io_wait();
	outb(PIC1_DATA, 0x20); //ICW2: Offset master PIC to 32-39
	io_wait();
	outb(PIC2_DATA, 0x28); //ICW2: Offset slave PIC to 40-47
	io_wait();
	outb(PIC1_DATA, 4); //ICW3: tell Master PIC that there is a slave PIC at IRQ2 (0000 0100)
	io_wait();
	outb(PIC2_DATA, 2); //ICW3: tell Slave PIC its cascade identity (0000 0010)
	io_wait();
	outb(PIC1_DATA, 1); //ICW4: 8086 mode
	io_wait();
	outb(PIC2_DATA, 1);
	io_wait();
	disable_all_irq();
}

void write_idt(uint8_t *dest, struct IDT *idt) {
	dest[0] = idt->handler & 0xFF;
	dest[1] = (idt->handler >> 8) & 0xFF;
	dest[2] = idt->selector & 0xFF;
        dest[3] = (idt->selector >> 8) & 0xFF;
	dest[4] = 0;
	dest[5] = idt->type;
	dest[6] = (idt->handler >> 16) & 0xFF;
	dest[7] = (idt->handler >> 24) & 0xFF;
}

extern void isr_wrapper();

void interrupt_handler() {
	kprintf("Inside interrupt handler\n");
	int irr = pic_get_irr();
	int isr = pic_get_isr();
	kprintf("%d, %d\n", irr, isr);
	if (irr == 2) {
		kprintf("Keyboard\n");
		int scan = inb(0x60);
		int i = inb(0x61);
		kprintf("%d\n", scan);
		kprintf("%d\n", i);
		outb(0x61, i|0x80);
		outb(0x61, i);
		asm("hlt");
	}
	send_eoi(1);
	disable_all_irq();
	//enable_irq(1);
	//asm("hlt");
}

void setup_idt() {
	static uint8_t system_idt[IDT_TOTAL_SIZE];
	static struct idt_ptr idtr;

	int i = 0;
        struct IDT default_idt = {.handler = (uint32_t) isr_wrapper, .selector = (uint16_t) 0x08, .type = 0x8F};
	for (i = 0; i < IDT_NUM; i++) {
		write_idt(system_idt + i * IDT_SIZE, &default_idt);
	}
	idtr.len = IDT_TOTAL_SIZE - 1;
	idtr.ptr = (uint32_t) &system_idt;
        asm("lidt (%0)" : : "m"(idtr));
	enable_irq(1);
        asm volatile("sti");
}

void enable_irq(int irq)
{
	uint16_t port;
	uint8_t value;
 
	if(irq < 8) {
		port = PIC1_DATA;
	} else {
		port = PIC2_DATA;
		irq -= 8;
	}
	value = inb(port) & ~(1 << irq);
	outb(port, value);
}

void disable_irq(int irq)
{
	uint16_t port;
	uint8_t value;
 
	if(irq < 8) {
		port = PIC1_DATA;
	} else {
		port = PIC2_DATA;
		irq -= 8;
	}
	value = inb(port) | (1 << irq);
	outb(port, value); 
}

void disable_all_irq()
{
	outb(PIC1_DATA, 0xFF);
        io_wait();
        outb(PIC2_DATA, 0xFF);
        io_wait();
}

void send_eoi(unsigned char irq)
{
	if(irq >= 8)
		outb(PIC2_COMMAND, PIC_EOI);
	outb(PIC1_COMMAND, PIC_EOI);
}

static uint16_t __pic_get_irq_reg(int ocw3)
{
	outb(PIC1_COMMAND, ocw3);
	outb(PIC2_COMMAND, ocw3);
	return (inb(PIC2_COMMAND) << 8) | inb(PIC1_COMMAND);
}
 
uint16_t pic_get_irr(void)
{
	return __pic_get_irq_reg(PIC_READ_IRR);
}
 
uint16_t pic_get_isr(void)
{
	return __pic_get_irq_reg(PIC_READ_ISR);
}

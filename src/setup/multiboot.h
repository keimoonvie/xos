#ifndef MULTIBOOT_H_
#define MULTIBOOT_H_

struct multiboot_info {
	uint32_t flags;
	uint32_t mem_lower;
	uint32_t mem_upper;
	uint32_t boot_device;
	uint32_t cmdline;
	uint32_t mods_count;
	uint32_t mods_addr;	
} __attribute__((packed));

#endif

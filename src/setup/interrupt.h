#ifndef INTERRUPT_H_
#define INTERRUPT_H_

#include <stdint.h>

void setup_intel8259();
void setup_idt();
void send_eoi(uint8_t irq);
void interrupt_handler();
void enable_irq(int irq);
void disable_irq(int irq);
void disable_all_irq();
uint16_t pic_get_irr(void);
uint16_t pic_get_isr(void);

#endif

#include <stdint.h>
#include "paging.h"

uint32_t page_dir[1024] __attribute__ ((aligned (4096)));

void init_paging() {
	void *page_dir_ptr = ((void *) page_dir) + 0x40000000;
	int i;
	for (i = 0; i < 1024; i++) {
		page_dir[i] = 0;
	}
	// Identity map 1st 4MB
	page_dir[0] = 0x83;
	// Map VM 3GB + 4MB to PM 4MB
	page_dir[(0xC0000000 >> 22)] = 0x83;

	asm volatile ("mov %0, %%eax\n"
                      "mov %%eax, %%cr3\n"
		      "mov %%cr4, %%ecx\n"
		      "orl $0x00000010, %%ecx\n"
		      "mov %%ecx, %%cr4\n"
                      "mov %%cr0, %%eax\n"
                      "orl $0x80000000, %%eax\n"
                      "mov %%eax, %%cr0\n" :: "m" (page_dir_ptr));
}

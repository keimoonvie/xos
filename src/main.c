#include "common/terminal.h"
#include "setup/multiboot.h"
#include "setup/paging.h"
#include "setup/gdt.h"
#include "setup/interrupt.h"

void change_segment(int x);

void kernel_main(struct multiboot_info *mb, unsigned int magic)
{
	init_paging();
	setup_gdt();
	terminal_init(make_color(COLOR_WHITE, COLOR_LIGHT_CYAN));
	kprintf("Hello, KERNEL\n");
	setup_intel8259();
	kprintf("Setting interrupt\n");
	setup_idt();
	kprintf("Done\n");
}

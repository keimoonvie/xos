#ifndef ASM_H_
#define ASM_H_

#include <stdint.h>

static inline int16_t read_cs() {
	int16_t ret;
	asm volatile ("mov %%cs, %0" : "=a"(ret));
	return ret;
}

static inline int16_t read_ds() {
	int16_t ret;
        asm volatile ("mov %%ds, %0" : "=a"(ret));
        return ret;
}

static inline int16_t read_es() {
	int16_t ret;
        asm volatile ("mov %%es, %0" : "=a"(ret));
        return ret;
}

static inline int32_t read_cr0() {
	int32_t ret;
	asm volatile ("movl %%cr0, %0" : "=a"(ret));
	return ret;
}

static inline void outb(uint16_t port, uint8_t val)
{
	asm volatile ("outb %0, %1" : : "a"(val), "Nd"(port));
}

static inline uint8_t inb(uint16_t port)
{
	uint8_t ret;
	asm volatile ("inb %1, %0" : "=a"(ret) : "Nd"(port));
	return ret;
}

static inline void io_wait(void)
{
	asm volatile ( "jmp 1f\n\t"
                   "1:jmp 2f\n\t"
		       "2:" );
}

#endif

#include "asm.h"
#include "terminal.h"

struct terminal {
	size_t column;
	size_t row;
	uint8_t color;
	uint16_t *buffer;
};

struct terminal _terminal;
static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;

uint8_t make_color(enum vga_color fg, enum vga_color bg)
{
	return fg | bg << 4;
}

uint16_t make_vgaentry(char c, uint8_t color)
{
	uint16_t c16 = c;
	uint16_t color16 = color;
	return c16 | color16 << 8;
}

void terminal_init(uint8_t color)
{
	_terminal.row = _terminal.column = 0;
	_terminal.color = color;
	_terminal.buffer = (uint16_t*) 0xB8000;
	size_t x, y = 0;
	for (y = 0; y < VGA_HEIGHT; y++) {
		for (x = 0; x < VGA_WIDTH; x++) {
			_terminal.buffer[y * VGA_WIDTH + x] = make_vgaentry(' ', color);
		}
	}
}

void terminal_putentryat(char c, uint8_t color, size_t x, size_t y)
{
	_terminal.buffer[y * VGA_WIDTH + x] = make_vgaentry(c, color);
}

void terminal_roll() {
	int x, y = 0;
	for (y = 0; y < VGA_HEIGHT - 1; y++) {       	
		for (x = 0; x < VGA_WIDTH; x++) {
			_terminal.buffer[y * VGA_WIDTH + x] = _terminal.buffer[(y + 1) * VGA_WIDTH + x];
		}
	}
	for (x = 0; x < VGA_WIDTH; x++) {
		terminal_putentryat(' ', _terminal.color, x, y);
	}
	_terminal.column = 0;
	_terminal.row = VGA_HEIGHT - 1;
}

void terminal_putchar(char c)
{
	if (c == '\n') {
		_terminal.column = 0;
		_terminal.row++;
	} else if (c == '\r') {
		_terminal.column = 0;
	} else {
		terminal_putentryat(c, _terminal.color, _terminal.column, _terminal.row);
		_terminal.column++;
	}
	if (_terminal.column == VGA_WIDTH) {
		_terminal.column = 0;
		_terminal.row++;
	}
	if (_terminal.row == VGA_HEIGHT) {
		terminal_roll();
	}
}

void terminal_putint(int value) {
	if (value < 0) {
		terminal_putchar('-');
		value = -value;
	}
	if (value < 10) {
		terminal_putchar((char) (value + '0'));
		return;
	}
	int number = value % 10;
	value = value / 10;
	terminal_putint(value);
	terminal_putchar((char) number + '0');
}

void terminal_putuint(uint32_t value) {
        if (value < 10) {
                terminal_putchar((char) (value + '0'));
                return;
        }
        int number = value % 10;
        value = value / 10;
        terminal_putuint(value);
        terminal_putchar((char) number + '0');
}

void terminal_putstring(const char *str) {
	const char *c = str;
	while (*c != 0) {
		terminal_putchar(*c);
		c++;
	}
}

void setcolor(uint8_t color)
{
	_terminal.color = color;
}

void terminal_update_cursor() {
	uint16_t position = ((uint16_t) _terminal.row * 80) + (uint16_t) _terminal.column;	
	outb(0x3D4, 0x0F);
	outb(0x3D5, (uint8_t) (position & 0xFF));
	outb(0x3D4, 0x0E);
	outb(0x3D5, (uint8_t) ((position >> 8) & 0xFF));
}

int kprintf(const char *format, ...)
{
	va_list ap;
	va_start(ap, format);
	int retval = vprintf(format, ap);
	va_end(ap);
	return retval;
}

int vprintf(const char *format, va_list ap)
{
	const char *c = format;
	while (*c != 0) {
		if (*c == '%') {
			switch (*(c + 1)) {
			case '%':
				terminal_putchar('%');
				c++;
				break;
			case 'c': {
				int val = va_arg(ap, int);
				terminal_putint(val);
				c++;
				break;
			}
			case 'd': {
				int val = va_arg(ap, int);
				terminal_putint(val);
				c++;
				break;
			}
			case 'D': {
				uint32_t val = va_arg(ap, uint32_t);
                                terminal_putuint(val);
                                c++;
                                break;
			}
			case 's': {
				char *val = va_arg(ap, char *);
				if (val) {
					terminal_putstring(val);
				}
				c++;
				break;
			}
			default:
				terminal_putchar(*c);
				break;

			}
		} else {
			terminal_putchar(*c);
		}
		c++;
	}
	terminal_update_cursor();
}
